from keras.layers import *
from utils import embedding_layer, SPECIAL_CHARS
from keras.models import Model


def Attention(attention_size=1, context_maxlen=600, answer_maxlen=5):
    def f(decoder_states, encoder_states):
        # Check dims
        #print(decoder_states.shape)
        #print(encoder_states.shape)
        decoder_states = Reshape((answer_maxlen, 512))(decoder_states)
        encoder_states = Reshape((context_maxlen, 512))(encoder_states)

        score_function = Dot(axes=(2, 2))([decoder_states, encoder_states])
        align_function = Activation('softmax', name="attention_alignment")(score_function)

        c = Dense(attention_size)(align_function)
        new_decoder_state = Concatenate(axis=-1)([c, decoder_states])
        new_decoder_state = Dense(512, activation="tanh")(new_decoder_state)
        return new_decoder_state
    return f

def Attention2():
    def f(decoder_states, encoder_states):
        #decoder_states = Reshape((5, 512))(decoder_states)
        #encoder_states = Reshape((600, 512))(encoder_states)
        attention = dot([decoder_states, encoder_states], axes=[2, 2])
        attention = Activation('softmax')(attention)
        print(attention.shape)
        context = dot([attention, encoder_states], axes=[2,1])
        print(context.shape)
        decoder_combined_context = Concatenate()([context, decoder_states])
        print(decoder_combined_context.shape)
        # Has another weight + tanh layer as described in equation (5) of the paper
        new_decoder_state = Dense(64, activation="tanh")(decoder_combined_context)
        print(new_decoder_state.shape)
        return new_decoder_state
    return f


class ModelGenerator(object):
    def __init__(self, context_maxlen, question_maxlen, answer_maxlen, word2idx, idx2word, word2glove):
        self.context_maxlen, self.question_maxlen, self.answer_maxlen = context_maxlen, question_maxlen, answer_maxlen
        self.word2idx, self.idx2word, self.word2glove = word2idx, idx2word, word2glove
        self.nvocab = len(self.word2idx.keys())
        self.embedding_size = 100 + len(SPECIAL_CHARS) #additional tokens

    def model_seq2seq(self):
        hidden_units = 256
        shared_embedding_layer = embedding_layer(self.word2glove, self.word2idx, False)

        context_inputs = Input(shape=(None, ), name='context_inputs')
        encoded_context = shared_embedding_layer(context_inputs)
        encoded_context = Dropout(0.3)(encoded_context)

        question_inputs = Input(shape=(None, ), name='question_inputs')
        encoded_question = shared_embedding_layer(question_inputs)
        encoded_question = Dropout(0.3)(encoded_question)

        encoded_question = LSTM(units=self.embedding_size, name='question_lstm')(encoded_question)
        encoded_question = RepeatVector(self.context_maxlen)(encoded_question)

        merged = add([encoded_context, encoded_question])
        encoder_outputs, encoder_state_h, encoder_state_c = LSTM(units=hidden_units,
                                                                 name='encoder_lstm', return_state=True)(merged)

        encoder_states = [encoder_state_h, encoder_state_c]

        decoder_inputs = Input(shape=(None, self.nvocab), name='decoder_inputs')
        decoder_lstm = LSTM(units=hidden_units, return_state=True, return_sequences=True, name='decoder_lstm')
        decoder_outputs, decoder_state_h, decoder_state_c = decoder_lstm(decoder_inputs,
                                                                         initial_state=encoder_states)
        decoder_dense = Dense(units=self.nvocab, activation='softmax', name='decoder_dense')
        decoder_outputs = decoder_dense(decoder_outputs)

        self.model = Model([context_inputs, question_inputs, decoder_inputs], decoder_outputs)

        self.model.summary()

        self.model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

        self.encoder_model = Model([context_inputs, question_inputs], encoder_states)

        decoder_state_inputs = [Input(shape=(hidden_units,)), Input(shape=(hidden_units,))]
        decoder_outputs, state_h, state_c = decoder_lstm(decoder_inputs, initial_state=decoder_state_inputs)
        decoder_states = [state_h, state_c]
        decoder_outputs = decoder_dense(decoder_outputs)
        self.decoder_model = Model([decoder_inputs] + decoder_state_inputs, [decoder_outputs] + decoder_states)
        return self.model

    def model_seq2seq_gru(self):
        hidden_units = 512
        shared_embedding_layer = embedding_layer(self.word2glove, self.word2idx, False, False)

        # -----------------------------FULL MODEL-----------------------------
        context_inputs = Input(shape=(self.context_maxlen,), name='context_inputs')
        encoded_context = shared_embedding_layer(context_inputs)
        #encoded_context = Dropout(0.3)(encoded_context)

        question_inputs = Input(shape=(self.question_maxlen,), name='question_inputs')
        encoded_question = shared_embedding_layer(question_inputs)
        #encoded_question = Dropout(0.3)(encoded_question)
        encoded_question = GRU(units=self.embedding_size, name='question_lstm')(encoded_question)
        encoded_question = RepeatVector(self.context_maxlen)(encoded_question)

        merged = Add()([encoded_context, encoded_question])
        print(merged.shape)
        output, state_h = GRU(units=hidden_units,
                return_state=True,
                name='encoder_lstm')(merged)


        layer_decoder_gru = GRU(units=hidden_units, return_sequences=True, return_state=True, name='decoder_lstm')
        layer_decoder_dense = Dense(units=self.nvocab, activation='softmax', name='decoder_dense')

        decoder_inputs = Input(shape=(None, self.nvocab), name='decoder_inputs')
        decoder_outputs, _ = layer_decoder_gru(decoder_inputs, initial_state=state_h)
        decoder_final = layer_decoder_dense(decoder_outputs)
        self.model = Model([context_inputs, question_inputs, decoder_inputs], decoder_final)

        # -----------------------------ENCODER-----------------------------
        self.encoder_model = Model([context_inputs, question_inputs], [state_h])

        # -----------------------------DECODER-----------------------------
        decoder_state_inputs = Input(shape=(hidden_units, ))
        decoder_outputs, decoder_state = layer_decoder_gru(decoder_inputs, initial_state=decoder_state_inputs)
        decoder_final = layer_decoder_dense(decoder_outputs)

        self.decoder_model = Model([decoder_inputs, decoder_state_inputs], [decoder_final, decoder_state])

        return self.model

    def model_seq2seq_gru_attention(self):
        print("Using GRU with attention...")
        hidden_units = 256
        shared_embedding_layer = embedding_layer(self.word2glove, self.word2idx, False, False)

        # -----------------------------FULL MODEL-----------------------------
        context_inputs = Input(shape=(self.context_maxlen,), name='context_inputs')
        encoded_context = shared_embedding_layer(context_inputs)

        question_inputs = Input(shape=(self.question_maxlen,), name='question_inputs')
        encoded_question = shared_embedding_layer(question_inputs)
        encoded_question = GRU(units=self.embedding_size, name='question_lstm')(encoded_question)
        encoded_question = RepeatVector(self.context_maxlen)(encoded_question)

        merged = Add()([encoded_context, encoded_question])
        encoder_outputs, encoder_state_f, encoder_state_b = Bidirectional(
            GRU(units=hidden_units,
                name='encoder_lstm',
                return_sequences=True,
                return_state=True),
            merge_mode="concat"
        )(merged)
        state_h = Concatenate()([encoder_state_f, encoder_state_b])

        layer_decoder_gru = GRU(units=hidden_units * 2, return_sequences=True, return_state=True, name='decoder_lstm')
        layer_decoder_dense = Dense(units=self.nvocab, activation='softmax', name='decoder_dense')
        #layer_attention = Attention(attention_size=64)
        layer_attention = Attention2()

        decoder_inputs = Input(shape=(None, self.nvocab), name='decoder_inputs')
        decoder_outputs, _ = layer_decoder_gru(decoder_inputs, initial_state=state_h)

        attended_decoder_outputs = layer_attention(decoder_outputs, encoder_outputs)
        decoder_final = layer_decoder_dense(attended_decoder_outputs)

        self.model = Model([context_inputs, question_inputs, decoder_inputs], decoder_final)

        # -----------------------------ENCODER-----------------------------
        self.encoder_model = Model([context_inputs, question_inputs], [encoder_outputs, state_h])

        # -----------------------------DECODER-----------------------------
        decoder_final_state_inputs = Input(shape=(hidden_units * 2,))
        decoder_all_state_inputs = Input(shape=(None, hidden_units * 2))

        decoder_outputs, decoder_state = layer_decoder_gru(decoder_inputs, initial_state=decoder_final_state_inputs)

        attended_decoder_outputs = layer_attention(decoder_outputs, decoder_all_state_inputs)
        decoder_final = layer_decoder_dense(attended_decoder_outputs)

        self.decoder_model = Model([decoder_inputs, decoder_all_state_inputs, decoder_final_state_inputs],
                                   [decoder_final, decoder_state])

        return self.model

    def baseline(self):
        hidden_units = 512
        shared_embedding_layer = embedding_layer(self.word2glove, self.word2idx, False, True)

        context_inputs = Input(shape=(self.context_maxlen,), name='context_inputs')
        encoded_context = shared_embedding_layer(context_inputs)
        decoder_gru1 = GRU(units=hidden_units, return_sequences=True, name='decoder_gru1')(encoded_context)

        question_inputs = Input(shape=(self.question_maxlen,), name='question_inputs')
        encoded_question = shared_embedding_layer(question_inputs)
        decoder_gru2 = GRU(units=hidden_units, return_sequences=True, name='decoder_gru2')(encoded_question)

        merged = Concatenate(axis=1)([decoder_gru1, decoder_gru2])

        preds = GRU(units=hidden_units, name='decoder_gru3')(merged)
        preds = RepeatVector(self.answer_maxlen)(preds)
        preds = GRU(units=hidden_units, return_sequences=True, name='decoder_gru4')(preds)
        preds = Dense(self.nvocab, activation='softmax')(preds)

        self.model = Model([context_inputs, question_inputs], preds)
        #self.model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        return self.model


    def load_model(self, name, type="gru"):
        if type == "baseline":
            self.baseline()
        elif type == "gru":
            self.model_seq2seq_gru()
        elif type == "simple":
            self.model_seq2seq()
        elif type == "attention":
            self.model_seq2seq_gru_attention()
        self.model.load_weights(name)
