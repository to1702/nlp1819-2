import os
import pickle
import nltk
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding
from keras import backend as K

SPECIAL_CHARS = [0, 0]
EMBEDDING_SIZE = 100

def get_unk(expand):
    unk = [-1.47727306e-03, 1.16986195e-02, 5.63374460e-02, -3.47573794e-02,
           -3.72531749e-02, 1.45927683e-01, -1.61246639e-02, 1.77925527e-02,
           3.35836783e-02, 1.80821698e-02, -4.79037873e-03, 4.25829589e-02,
           5.18651642e-02, -2.27106884e-02, 9.35870856e-02, -3.66204605e-02,
           3.20284553e-02, 3.64387706e-02, -5.28916996e-03, -1.58817950e-03,
           1.31571982e-02, -1.23270163e-02, 5.22907190e-02, 4.26204540e-02,
           1.20019183e-01, -1.34906106e-04, -2.84356084e-02, -8.94949865e-03,
           -3.72861959e-02, -7.10025653e-02, -2.21453924e-02, 1.33193890e-03,
           -1.63850971e-02, 9.36579797e-03, -4.45674807e-02, 9.59800258e-02,
           -1.11470921e-02, 1.67635251e-02, 1.05529418e-02, -5.39707169e-02,
           -4.54823524e-02, 8.05339441e-02, -1.87118948e-02, 5.21890400e-03,
           -8.68741870e-02, 2.56631095e-02, 1.30983442e-01, -9.07369051e-03,
           2.98847165e-02, -1.26251159e-02, -7.74169937e-02, 8.72649252e-03,
           4.60509956e-02, 1.96382701e-01, -1.13684408e-01, -2.71354020e-01,
           6.09643981e-02, -5.90543523e-02, 6.85758367e-02, 2.82638390e-02,
           -2.29146611e-02, 1.59488365e-01, -5.14013972e-03, 9.14586335e-02,
           7.70418420e-02, -4.49751057e-02, 1.05513856e-01, -1.80662759e-02,
           6.20385632e-02, -9.82260481e-02, -7.49971514e-05, -6.18130565e-02,
           5.00478223e-02, -5.05499057e-02, -4.66226749e-02, 8.59108288e-03,
           -2.07145475e-02, -2.59777084e-02, -1.20968580e-01, -5.06046638e-02,
           2.77289227e-02, -2.34290157e-02, -6.73430115e-02, 1.25452280e-01,
           -1.93837985e-01, -2.93775350e-02, 4.14730795e-02, -9.99642350e-03,
           -1.03275068e-01, -8.36013928e-02, -4.16922756e-02, 1.03302533e-02,
           -1.52007909e-02, 1.77519210e-02, -1.52420187e-02, -9.79247987e-02,
           -5.33485822e-02, -7.40055293e-02, 3.41406139e-03, 5.63698821e-02]
    if expand:
        return np.array(unk + SPECIAL_CHARS, dtype=np.float64)
    return np.array(unk, dtype=np.float(64))

def get_special_char(idx):
    holder = SPECIAL_CHARS
    holder[idx] = 1
    return np.array(EMBEDDING_SIZE*[0] + holder, dtype=np.float64)

def to_hotone(idx, nvocab):
    vec = [0]*nvocab
    vec[idx] = 1
    return vec

def to_idx(word, word_idx):
    if word in word_idx:
        return word_idx[word]
    return word_idx['<unk>']


# Prepare Glove File
def read_glove(glove_file, reset=False):
    print("Searching for gloVe file..")
    if os.path.isfile("wordvecs/glove_processed.pkl") and not reset:
        with open("wordvecs/glove_processed.pkl", "rb") as fp:
            print("GloVe file found.")
            return pickle.load(fp)

    print("Not found, generating new gloVe file..")
    with open(glove_file, 'r') as f:
        word2glove = {}  # map from a token (word) to a Glove embedding vector
        word2idx = {}  # map from a token to an index
        idx2word = {}  # map from an index to a token

        for line in f:
            record = line.strip().split()
            # take the token (word) from the text line
            token = record[0]
            word2glove[token] = np.array(record[1:] + SPECIAL_CHARS,
                                         dtype=np.float64)  # associate the Glove embedding vector to a that token (word)

        word2glove['<unk>'] = get_unk(expand=True)
        word2glove['<start>'] = get_special_char(0)
        word2glove['<stop>'] = get_special_char(1)
        #word2glove['<q>'] = get_special_char(2)

        tokens = sorted(word2glove.keys())
        for idx, tok in enumerate(tokens):
            keras_idx = idx + 1  # 0 is reserved for masking in Keras (see above)
            word2idx[tok] = keras_idx  # associate an index to a token (word)
            idx2word[keras_idx] = tok  # associate a word to a token (word). Note: inverse of dictionary above

    with open("wordvecs/glove_processed.pkl", "wb") as fp:
        pickle.dump((word2idx, idx2word, word2glove), fp)
    return word2idx, idx2word, word2glove


def vectorize_stories_seq2seq(data, word_idx, idx2word, story_maxlen, query_maxlen, answer_maxlen):
    nvocab = len(word_idx.keys())
    xs = []
    xqs = []
    entry_ids = []
    turn_ids = []
    ys1 = np.zeros(shape=(len(data), answer_maxlen, nvocab))
    ys2 = np.zeros(shape=(len(data), answer_maxlen, nvocab))
    for i, (story, query, answer, entry_id, turn_id) in enumerate(data):
        x = [to_idx(w, word_idx) for w in nltk.word_tokenize(story.lower())]
        xq = [to_idx(w, word_idx) for w in nltk.word_tokenize(query.lower())]
        y = [to_idx(w, word_idx) for w in nltk.word_tokenize(answer.lower())]

        # let's not forget that index 0 is reserved

        # y = np.zeros(len(word_idx) + 1)
        # y[word_idx[answer]] = 1
        # if i % 1000 == 0:
        #    print("At " + str(i))

        for si, widx in enumerate([word_idx['<start>']] + y):

            if si >= answer_maxlen:
                continue
            #print(i, si, widx, idx2word[widx])

            ys1[i, si, widx] = 1
        for si, widx in enumerate(y + [word_idx['<stop>']]):

            if si == answer_maxlen-1:
                ys1[i, si, word_idx['<stop>']] = 1
                #print(i, si, widx, idx2word[word_idx['<stop>']])
                break
            #print(i, si, widx, idx2word[widx])
            ys2[i, si, widx] = 1

        ## add question tag
        # xq = [word_idx['<q>']] + xq
        #print(xq)
        xs.append(x)
        xqs.append(xq)
        entry_ids.append(entry_id)
        turn_ids.append(turn_id)

    return (pad_sequences(xs, maxlen=story_maxlen),
            pad_sequences(xqs, maxlen=query_maxlen),
            ys1, ys2, entry_ids, turn_ids)

def vectorize_stories(data, word_idx, idx2word, story_maxlen, query_maxlen, answer_maxlen):
    nvocab = len(word_idx.keys())
    xs = []
    xqs = []
    entry_ids = []
    turn_ids = []
    ys = np.zeros(shape=(len(data), answer_maxlen, nvocab))
    for i, (story, query, answer, entry_id, turn_id) in enumerate(data):
        x = [to_idx(w, word_idx) for w in nltk.word_tokenize(story.lower())]
        xq = [to_idx(w, word_idx) for w in nltk.word_tokenize(query.lower())]
        y = [to_idx(w, word_idx) for w in nltk.word_tokenize(answer.lower())]

        for si, widx in enumerate(y):

            if si >= answer_maxlen:
                continue
            #print(i, si, widx, idx2word[widx])

            ys[i, si, widx] = 1

        xs.append(x)
        xqs.append(xq)
        entry_ids.append(entry_id)
        turn_ids.append(turn_id)

    return (pad_sequences(xs, maxlen=story_maxlen),
            pad_sequences(xqs, maxlen=query_maxlen),
            ys, entry_ids, turn_ids)


# Create Pretrained Keras Embedding Layer
def embedding_layer(word2glove, word2idx, trainable, mask):
    def f(x):
        n_vocab = len(word2idx) + 1  # adding 1 to account for masking
        dims = next(iter(word2glove.values())).shape[0]  # works with any glove dimensions (e.g. 50)

        embedding_mat = np.zeros((n_vocab, dims))  # initialize with zeros
        for word, index in word2idx.items():
            embedding_mat[index, :] = word2glove[word]  # create embedding: word index to Glove word embedding

        return Embedding(n_vocab, dims, weights=[embedding_mat], trainable=trainable, mask_zero=mask)(x)

    return f
