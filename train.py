from keras.callbacks import ModelCheckpoint
from model import ModelGenerator
from keras.optimizers import Adam
import pickle
from random import shuffle
from utils import read_glove, vectorize_stories, vectorize_stories_seq2seq

def data_gen(data, batch_size):
    while True:
        shuffle(data)
        for i in range(0, len(data), batch_size):
            if i + batch_size > len(data):
                break
            x, xq, y, _, _ = vectorize_stories(data[i:i + batch_size], word2idx, idx2word, context_maxlen, question_maxlen,
                                                 answer_maxlen)
            yield [x, xq], y



            # tx, txq, ty = vectorize_stories(test, word2idx, context_maxlen, question_maxlen, answer_maxlen)

def data_gen_seq2seq(data, batch_size):
    while True:
        shuffle(data)
        for i in range(0, len(data), batch_size):
            if i + batch_size > len(data):
                break

            x, xq, y_input, y_target, _, _ = vectorize_stories_seq2seq(data[i:i + batch_size], word2idx, idx2word, context_maxlen, question_maxlen, answer_maxlen)
            #print(y_input.shape)
            #print(y_target.shape)
            yield [x, xq, y_input], y_target



context_maxlen = 600
question_maxlen = 40
answer_maxlen = 5

# GLOVE_FILE = "glove.42B.300d.txt"
GLOVE_FILE = "wordvecs/glove.6B.100d.filtered.txt"
word2idx, idx2word, word2glove = read_glove(GLOVE_FILE)

mg = ModelGenerator(context_maxlen, question_maxlen, answer_maxlen, word2idx, idx2word, word2glove)
#model = mg.model_seq2seq_gru()
model = mg.model_seq2seq_gru_attention()
#model = mg.baseline()

#model = model_v1(context_maxlen, question_maxlen, answer_maxlen, word2glove, word2idx)

optimizer = "rmsprop"
#optimizer = "adam"
#optimizer = Adam(lr=0.001)
model.compile(optimizer=optimizer,
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.summary()

with open("data/coqa-train", "rb") as f:
    train_data = pickle.load(f)

with open("data/coqa-dev", "rb") as f:
    dev_data = pickle.load(f)

EPOCHS = 100
BATCH_SIZE = 128
TRAIN_STEPS = len(train_data) // BATCH_SIZE
DEV_STEPS = len(dev_data) // BATCH_SIZE


callbacks = [
    ModelCheckpoint("best_weights.h5", monitor="val_loss", save_best_only=True)
]

print('-- Training --')
print("- Data sizes: train (" + str(len(train_data)) + "), test (" + str(len(dev_data)) + ")")
print("- Batch size: " + str(BATCH_SIZE))
print("- Train steps: " + str(TRAIN_STEPS))
print("- Test steps: " + str(DEV_STEPS))
model.fit_generator(data_gen_seq2seq(train_data, BATCH_SIZE), TRAIN_STEPS,
                    validation_data=data_gen_seq2seq(dev_data, BATCH_SIZE),
                    validation_steps=DEV_STEPS, callbacks=callbacks, epochs=EPOCHS)

#print('Evaluation')
#loss, acc = model.evaluate([tx, txq], ty,
#                           batch_size=BATCH_SIZE)
#print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))
