from utils import read_glove, vectorize_stories_seq2seq, embedding_layer, vectorize_stories
import pickle
import numpy as np
import json
from tqdm import tqdm
from model import ModelGenerator
from keras.models import load_model

def to_sentence(idx_list):
    if len(idx_list.shape) > 2:
        return " ".join([idx2word[x[0]] if x[0] != 0 else "" for x in idx_list[0] ])
    return " ".join([idx2word[x] if x != 0 else "" for x in idx_list[0]])

def predict_simple(mg, paragraph, question):
    states_value = mg.model.predict([paragraph, question])
    seq = []
    for i in range(answer_maxlen):
        seq.append(np.argmax(states_value[0][i]))

    sentence = " ".join([idx2word[x] for x in seq])
    #print(idx2word[0])
    return sentence

def predict(mg, paragraph, question):
    encoder_state = mg.encoder_model.predict([paragraph, question])
    encoder_state = [encoder_state]
    #encoder_state = np.expand_dims(encoder_state, axis=1)
    batch_size = len(paragraph)
    target_seq = np.zeros((batch_size, 1, mg.nvocab))
    target_seq[:, 0, mg.word2idx['<start>']] = 1

    target_text_len = 0
    terminated = [False] * batch_size
    target_text = [[] for _ in range(batch_size)]

    while not all(terminated):
        output_tokens, decoder_state = mg.decoder_model.predict([target_seq] + encoder_state)

        #print(output_tokens.shape)
        sample_token_idx = np.argmax(output_tokens[:, -1, :], axis=-1)
        target_seq = np.zeros((batch_size, 1, mg.nvocab))

        for n_sent in range(batch_size):

            if terminated[n_sent]:
                continue

            sample_word = mg.idx2word[sample_token_idx[n_sent]]
            #print(sample_word)
            target_text_len += 1

            if sample_word != '<start>' and sample_word != '<stop>':
                target_text[n_sent].append(sample_word)

            if sample_word == '<stop>' or target_text_len >= mg.answer_maxlen:
                terminated[n_sent] = True

            target_seq[n_sent, 0, sample_token_idx[n_sent]] = 1

        encoder_state = [decoder_state]

    return [" ".join(tt) for tt in target_text]



context_maxlen = 800
question_maxlen = 100
answer_maxlen = 5
GLOVE_FILE = "glove.6B.100d.filtered.txt"
word2idx, idx2word, word2glove = read_glove("coqa-baselines/wordvecs/" + GLOVE_FILE)

with open("data/coqa-dev", "rb") as f:
    dev_data = pickle.load(f)

mg = ModelGenerator(context_maxlen, question_maxlen, answer_maxlen, word2idx, idx2word, word2glove)
#model = get_model()

#mg.load_model("best_weights.h5", type="attention")
mg.load_model("best_weights.h5", type="gru")
mg.model.summary()

results = []

batch = 128
print(len(dev_data))

for i in tqdm(range(0, len(dev_data), batch)):

    entry = dev_data[i:i+batch]

    x, xq, y1, y2, entry_ids, turn_ids = vectorize_stories_seq2seq(entry, word2idx, idx2word, context_maxlen, question_maxlen, answer_maxlen)

    sentance = predict(mg, x, xq)
    #print(entry[0][0])
    #print(entry[0][1])
    #print(entry[0][2])
    #print(" ".join([idx2word[np.where(vec==1)[0][0]] for vec in y1[0] if len(np.where(vec==1)[0]) > 0]))
    #print(sentance)
    #print("--------------------------------------------------")
    for p, q, s, en_id, tu_id in zip(x, xq, sentance, entry_ids, turn_ids):
        results.append({"id": en_id, "turn_id": tu_id, "answer": s})



with open("predictions.json", "w") as fp:
    json.dump(results, fp)

"""
    data_set = SquADDataSet(data_path='./data/squad/dev-v2.0.json')
    for i in range(20):
        index = i * 10
        paragraph, question, actual_answer = data_set.get_data(index)
        predicted_answer = qa.reply(paragraph, question)
        print('context: ', paragraph)
        print('question: ', question)
        print({'guessed_answer': predicted_answer, 'actual_answer': actual_answer})



for element in dev_data:
    x, xq, y = vectorize_stories([element], word2idx, context_maxlen, question_maxlen, answer_maxlen)

    #print(to_sentence(x))
    #print(to_sentence(xq))
    #print(to_sentence(y))

    result = model.predict([x, xq])
    #print(result[0][0]))
    res_vec = np.array([[np.argmax(x)] for x in result[0]])
    #print(res_vec)
    print(to_sentence(res_vec))
    sleep(0.5)
"""