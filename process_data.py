import argparse
import os
import json
import pickle
import nltk
import numpy as np
from utils import read_glove

def parse_args():
    parser = argparse.ArgumentParser(description='Data processing...')
    subparsers = parser.add_subparsers(help='commands')

    # A list command
    data_parser = subparsers.add_parser('data', help='Data manipulation')
    data_parser.set_defaults(which='data')
    data_parser.add_argument('--dtype', type=str, choices=['dev', 'train', 'both'], help='Which data? [dev, train, both]')
    data_parser.add_argument('--mode', type=str, choices=['extract', 'filter', 'expand', 'pipeline'], help='What to do? [extract, filter, expand]', required=True)

    embedding_parser = subparsers.add_parser('emb', help='Embedding manipulation')
    embedding_parser.set_defaults(which='emb')
    embedding_parser.add_argument('--mean', action='store_true', help='Show embedding avg')
    embedding_parser.add_argument('--filter', action='store_true', help='Filter embeddings')
    embedding_parser.add_argument('--exists', type=str, help='Check if exists')
    embedding_parser.add_argument('--reset', action='store_true', help='Reset embeddings')
    return parser.parse_args()


def print_segment(data, idx):
    print(data[idx]["story"])
    for n in range(len(data[idx]["questions"])):
        print(data[idx]["questions"][n]["input_text"])
        print(data[idx]["answers"][n]["input_text"])
        print()


def extract_single(data, idx):
    passage = data[idx]["story"]
    entry_id = data[idx]["id"]
    a, q = [], []
    turn_ids = []
    for n in range(len(data[idx]["questions"])):
        q.append(data[idx]["questions"][n]["input_text"])
        a.append(data[idx]["answers"][n]["input_text"])
        turn_ids.append(data[idx]["questions"][n]["turn_id"])
    return {"passage": passage,
            "questions": q,
            "answers": a,
            "turn_ids": turn_ids,
            "entry_id": entry_id}


def expand_data(mode):
    with open('data/coqa-' + mode + '-custom.json', 'r') as fp:
        data = json.load(fp)

    new_data = []
    for element in data:
        passage = element['passage']
        entry_id = element['entry_id']
        for i in range(len(element['questions'])):
            question = element['questions'][i]
            answer = element['answers'][i]
            turn_id = element['turn_ids'][i]

            new_data.append([passage, question, answer, entry_id, turn_id])
            passage += " " + question + " " + answer + "."

    with open("data/coqa-" + mode, "wb") as f:
        pickle.dump(new_data, f)


def extract_data(mode):
    with open('data/coqa-' + mode + '-v1.0.json', 'r') as fp:
        data = json.load(fp)["data"]

    dataset = []
    for i in range(len(data)):
        d = extract_single(data, i)
        dataset.append(d)

    with open('data/coqa-' + mode + '-custom.json', 'w') as fp:
        json.dump(dataset, fp)

def filter_data(mode):
    with open('data/coqa-' + mode + '-custom.json', 'r') as fp:
        data = json.load(fp)

    lengths = []
    for element in data:

        passage = element['passage']
        lengths.append(len(passage.split(" ")))
        #for q, a in zip(element['questions'], element['answers']):
        #    lengths.append(len(a.split(" ")))

    print(np.sum(lengths)/len(lengths))

def filter_embeddings():
    glove_path = "wordvecs/glove.6B.100d.txt"
    new_glove_path = "wordvecs/glove.6B.100d.filtered.txt"
    with open(glove_path, "r") as fp:
        embeddings = fp.read()

    train_file_path = "data/coqa-train-custom.json"
    with open(train_file_path, 'r') as fp:
        data = json.load(fp)

    all_words = []
    i=0
    for entry in data:
        #if i == 100:
        #    break
        #i+=1
        all_words += nltk.word_tokenize(entry['passage'].lower())
        for q, a in zip(entry['questions'], entry['answers']):
            all_words += nltk.word_tokenize(q.lower()) + nltk.word_tokenize(a.lower())


    freq = nltk.FreqDist(all_words)
    new_words = [k for k in freq.keys() if freq[k] > 1]

    print("Old vocabulary: " + str(len(set(all_words))))
    print("New vocabulary: " + str(len(new_words)))
    print("Reduction coeff: " + str(len(new_words)/len(set(all_words))))

    new_lines = []
    print("Filtering...")
    for line in embeddings.split("\n"):
        emb = line.split(" ")[0]
        if emb in new_words:
            new_lines.append(line)

    print("Size of new vocab: " + str(len(new_lines)))
    print("Saving new embeddings...")
    new_embeddings = "\n".join(new_lines)
    with open(new_glove_path, "w") as fp:
        fp.write(new_embeddings)

    #print(len(embedding_words.intersection(all_words)))

    #new_embeddings = []
    #for line in data.split("\n"):
    #    word = line.split(" ")[0]


def get_mean_emb():
    new_glove_path = "wordvecs/glove.6B.100d.filtered.txt"

    # Get number of vectors and hidden dim
    with open(new_glove_path, 'r') as f:
        for i, line in enumerate(f):
            pass
    n_vec = i + 1
    hidden_dim = len(line.split(' ')) - 1

    vecs = np.zeros((n_vec, hidden_dim), dtype=np.float32)

    with open(new_glove_path, 'r') as f:
        for i, line in enumerate(f):
            vecs[i] = np.array([float(n) for n in line.split(' ')[1:]], dtype=np.float32)

    return repr(np.mean(vecs, axis=0))


def embedding_exists(word):
    filename = "wordvecs/glove_processed.pkl"
    word2idx, idx2word, word2glove = read_glove(filename)
    if word in word2idx.keys():
        return True, word2idx[word], word2glove[word]
    return False, None, None

def reset():
    filename = "wordvecs/glove.6B.100d.filtered.txt"
    read_glove(filename, True)

if __name__ == "__main__":
    args = parse_args()
    print(args)
    if args.which == "data":
        if args.mode == "filter":
            filter_data(args.dtype)
        elif args.mode == "extract":
            extract_data(args.dtype)
        elif args.mode == "expand":
            expand_data(args.dtype)
        elif args.mode == "pipeline":
            if args.dtype == "both":
                extract_data("train")
                expand_data("train")
                extract_data("dev")
                expand_data("dev")

    else:
        if args.filter:
            filter_embeddings()
        elif args.mean:
            print(get_mean_emb())
        elif args.reset:
            reset()
        else: print(embedding_exists(args.exists))