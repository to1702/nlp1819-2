# NLP: CoQA challenge 

A sequence to sequence model implementation for QA

## Instructions 
1. Install dependancies: `pip install -r requirements.txt`
2. Create a data folder `mkdir data`.
3. Download coqa-train-v1.0.json and coqa-dev-v1.0.json to `data/` folder. 
4. Run `python process_data.py data --mode pipeline --dtype both` to expand 
and prepare the data. This will create files data/coqa-train and data/coqa-dev.


##### To train the model:  
Run `python train.py`. 

##### To test the model:  
Run `python test.py`. Results will be saved to predictions.json
To calculate the F1 score, run: 
`python evaluate-v1.0.py --data-file data/coqa-dev-v1.0.json  --pred-file predictions.json --verbose --out-file eval.json`


Download pre-trained model at: 
https://drive.google.com/open?id=11nTg26Tnyq4hR1rDu-ZX_SnMQKwhE2QI